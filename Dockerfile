FROM ruby:3-alpine
LABEL maintainer="me@nils.bartels.xyz"
ENV APPHOME=/app

# Add Alpine packages
RUN apk add --update --no-cache \
      bind-tools \
      curl \
      file \
      nano \
      nmap \
      openssh-client \
      tmux \
      tzdata \
      vim \
      wget

# Add user
RUN addgroup -g 1000 -S app && \
    adduser -u 1000 -S app -G app --home ${APPHOME} --shell /bin/ash
USER app

WORKDIR ${APPHOME}

RUN gem install tmuxinator && \
    mkdir -p ${APPHOME}/.config/tmuxinator
ADD base.yml ${APPHOME}/.config/tmuxinator/

# Start up
ENTRYPOINT ["tmuxinator"]
CMD ["start", "base"]
